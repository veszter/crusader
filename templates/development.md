
## Remote source code repository
Remote repository is available on bitbucket.org
Go to bitbucket.org and log in with google account.

## Heroku hosted application on kg-crusader.herokuapp.com
Onboarded users

 * admin
 * veszter
 * demouser
 For passwords go to local keepass2. 

 ## Local development
Start commander.
> pipenv shell
> manage.py runserver

### Local test user
admin/admin
beta/betabeta

## Administration surface
/admin

