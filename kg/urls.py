from django.urls import path
from .views import KgPostView, StatsView, LogoutSuccessView
from .views import StatsView2


urlpatterns = [
    path('', KgPostView.as_view(), name='kg'),
    path('logout_success/',
         LogoutSuccessView.as_view(),
         name='logout-success'),
    # path('stats/', StatsView.as_view(), name='stats'),
    path('stats/', StatsView2.as_view(), name='stats'),
]
