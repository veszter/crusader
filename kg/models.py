from django.db import models
# from django.utils import timezone
from django.contrib.auth import get_user_model


class Kg(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, null=True)
    kg = models.FloatField()
    date = models.DateTimeField()

    def __str__(self):
        return '%f' % self.kg

