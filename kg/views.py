from django.shortcuts import render, reverse
from .models import Kg
from django.views import generic
from django.http import HttpResponseRedirect, HttpResponse
from django.utils import timezone
from django.contrib.auth.mixins import LoginRequiredMixin


class KgPostView(LoginRequiredMixin, generic.TemplateView):
    login_url = '/login'

    def get(self, request, **kwargs):
        prev = Kg.objects.order_by('-date').first
        if not prev:
            prev = ''
        context = {
            'prev': prev,
        }
        return render(request, 'kg/kg.html', context)

    def post(self, request, **kwargs):
        new_kg = request.POST['kg']
        user = request.user
        if not user.is_authenticated:
            return HttpResponse(status=401, reason="You need to log in")
        kg = Kg(user=user, kg=new_kg, date=timezone.now())
        kg.save()
        return HttpResponseRedirect(reverse('stats'))


class StatsView(LoginRequiredMixin, generic.ListView):
    login_url = '/login'
    template_name = 'kg/stats.html'
    queryset = Kg.objects.all()
    context_object_name = 'kgs'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['kgs'] = Kg.objects.filter(user=self.request.user).order_by('-date')
        return context


class StatsView2(LoginRequiredMixin, generic.ListView):
    login_url = '/login'
    template_name = 'kg/stats2.html'
    queryset = Kg.objects.all()
    context_object_name = 'kgs'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['kgs'] = Kg.objects.filter(user=self.request.user).order_by('-date')
        return context


class LogoutSuccessView(generic.TemplateView):
    template_name = 'logout_success.html'
